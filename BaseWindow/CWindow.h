#ifndef CWindow_HPP
#define CWindow_HPP

#include <Windows.h>
#include <string>

#ifdef _MSC_VER	// Only working on Visual Studio
#   pragma comment(linker, "/subsystem:windows /ENTRY:mainCRTStartup")
#	pragma comment(lib, "user32.lib")
#endif

class CWindow
{
	protected:
		HWND		hWnd;
		std::wstring	Title;

		int			Width;
		int			Height;

	public:

		CWindow(const std::wstring Title, const int Width, const int Height) : Title(Title), Width(Width), Height(Height) {};

		virtual int create();
		virtual void destroy();

		int run();

	private:
		static LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

};

#endif