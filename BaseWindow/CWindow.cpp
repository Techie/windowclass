#ifndef CWindow_CPP
#define CWindow_CPP

#include "CWindow.h"

int CWindow::create()
{
	WNDCLASSEX wc;
	ZeroMemory(&wc, sizeof(WNDCLASSEX));
	wc.cbSize = sizeof(WNDCLASSEX);
	wc.hInstance = GetModuleHandle(NULL);
	wc.lpfnWndProc = CWindow::WndProc;
	wc.lpszClassName = Title.c_str();

	if (!RegisterClassEx(&wc))
	{
		MessageBox(NULL, L"Failed to register class!", L"Error!", MB_OK);
		exit(1);
	}

	hWnd = CreateWindowEx(NULL,
		Title.c_str(),
		Title.c_str(),
		WS_OVERLAPPEDWINDOW,
		600,
		600,
		Width,
		Height,
		NULL,
		NULL,
		GetModuleHandle(NULL),
		this);

	// After all a simple creation of a window.
	// However, the last parameter has to be "this"
	// As it is going to be the reference of our class 
	// passed to the static WndProc method.

	if (!hWnd)
	{
		MessageBox(NULL, L"Failed to create Window!", L"Error!", MB_OK);
		return -1;
	}

	ShowWindow(hWnd, SW_SHOWDEFAULT);
	return 0;
}

void CWindow::destroy()
{
	DestroyWindow(hWnd);
}


int CWindow::run()
{
	MSG msg;
	while (PeekMessage( &msg,hWnd, 0, 0, PM_REMOVE))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);

		if (msg.message == WM_QUIT)
			return 0;
	}
	return 1;
}

LRESULT CWindow::WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	CWindow* pWin = NULL;

	if (msg == WM_NCCREATE){
		SetWindowLongPtr(hWnd, GWLP_USERDATA, (long)(LPCREATESTRUCT(lParam)->lpCreateParams));
	}

	pWin = (CWindow*)GetWindowLongPtr(hWnd, GWLP_USERDATA);
	// Here we retrieve our classreference

	switch (msg)
	{
	case WM_CLOSE:
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
	};
	
	return DefWindowProc(hWnd, msg, wParam, lParam);
}



#endif