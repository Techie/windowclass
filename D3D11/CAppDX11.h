#ifndef CAPPDX11_H
#define CAPPDX11_H

#include "CWindow.h"
#include <d3d11.h>

#pragma comment (lib, "d3d11.lib")

class CAppDX11 : public CWindow
{
	private:
		IDXGISwapChain *swapchain;
		ID3D11Device *dev;                 
		ID3D11DeviceContext *devcon;

		bool Fullscreen;

	public:
		CAppDX11(const std::wstring Title, const int Width, const int Height, const bool Fullscreen) : CWindow(Title, Width, Height), Fullscreen(Fullscreen) {};

		int create();
		void destroy();

};

#endif