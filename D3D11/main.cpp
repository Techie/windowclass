#ifndef MAIN_CPP
#define MAIN_CPP

#include "CAppDX11.h"

int main()
{
	CAppDX11 app( L"Example DirectX 11 Window", 600, 400, false);
	app.create();

	// To be honest, it's 1:20 AM
	// and I don't have too much expearence with DirectX 11

	IDXGISwapChain *swapchain = app.getSwapChain();
	ID3D11DeviceContext *devcon = app.getDeviceContext();
	ID3D11RenderTargetView *backbuffer = app.getTargetView();

	while (app.run())
	{

		float clearColor[4] = { 0.0f, 0.0f, 0.25f, 1.0f };
		devcon->ClearRenderTargetView(backbuffer, clearColor);

		// do 3D rendering on the back buffer here

		// switch the back buffer and the front buffer
		swapchain->Present(0, 0);
	}
	return 0;
}

#endif