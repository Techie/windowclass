#ifndef CAPPDX11_CPP
#define CAPPDX11_CPP

#include "CAppDX11.h"

int CAppDX11::create()
{
	CWindow::create();

	DXGI_SWAP_CHAIN_DESC chain;
	ZeroMemory(&chain, sizeof(DXGI_SWAP_CHAIN_DESC));

	chain.BufferCount = 2;									// Double Buffering ?
	chain.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;	// 32 bit colours
	chain.OutputWindow = hWnd;                              // The window to be used
	chain.SampleDesc.Count = 4;                             // How many multisamples
	chain.Windowed = (BOOL)!Fullscreen;

	HRESULT result = D3D11CreateDeviceAndSwapChain(NULL,
		D3D_DRIVER_TYPE_HARDWARE,
		NULL,
		NULL,
		NULL,
		NULL,
		D3D11_SDK_VERSION,
		&chain,
		&swapchain,
		&dev,
		NULL,
		&devcon);

	if (result == S_OK)
		return 0;
	return -1;

}

void CAppDX11::destroy()
{
	swapchain->Release();
	dev->Release();
	devcon->Release();

	CWindow::destroy();
}

#endif