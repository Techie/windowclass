#ifndef CAPPDX9_HPP
#define CAPPDX9_HPP

#ifdef _MSC_VER
#pragma comment( lib, "d3d9.lib")
#endif

#include <d3d9.h>
#include "CWindow.h"

class CAppDX9 : public CWindow
{
	private:
		LPDIRECT3D9			pD3D;
		LPDIRECT3DDEVICE9	pD3DDev;

		bool Fullscreen;

	public:
		
		CAppDX9(const std::wstring Title, const int Width, const int Height, const bool Fullscreen) : CWindow(Title, Width, Height), Fullscreen(Fullscreen){};

		int create();
		void destroy();

		

		LPDIRECT3D9 getD3D();
		LPDIRECT3DDEVICE9 getD3DDevice();

};

#endif