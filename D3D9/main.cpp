#ifndef MAIN_CPP
#define MAIN_CPP

#include "CWindow.h"

int main()
{
	CWindow app(L"Example Window", 300, 300);
	app.create();
	while (app.run());
	return 0;
}

#endif