#ifndef CAPPDX9_CPP
#define CAPPDX9_CPP

#include "CAppDX9.h"

int CAppDX9::create()
{
	CWindow::create();

	pD3D = Direct3DCreate9(D3D_SDK_VERSION);

	D3DPRESENT_PARAMETERS d3dpp;
	ZeroMemory(&d3dpp, sizeof(d3dpp));
	d3dpp.Windowed		= (BOOL) !Fullscreen;
	d3dpp.SwapEffect	= D3DSWAPEFFECT_DISCARD;
	d3dpp.hDeviceWindow = hWnd;

	HRESULT val = pD3D->CreateDevice(D3DADAPTER_DEFAULT,
		D3DDEVTYPE_HAL,
		hWnd,
		D3DCREATE_SOFTWARE_VERTEXPROCESSING,
		&d3dpp,
		&pD3DDev
		);

	if (val == D3D_OK)
		return 0;
	return -1;

}

void CAppDX9::destroy()
{
	pD3DDev->Release();
	pD3D->Release();
	
	CWindow::destroy();

}

LPDIRECT3D9 CAppDX9::getD3D()
{
	return pD3D;
}

LPDIRECT3DDEVICE9 CAppDX9::getD3DDevice()
{
	return pD3DDev;
}

#endif